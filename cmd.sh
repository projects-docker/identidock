#!/usr/bin/env bash

set -e 

#
# Test for whats web server running.
#
if [ "${ENV}" = 'DEV' ]; then
  echo -e "\nRunning Development Server"
  exec python3 "identidock.py"
else
  echo -e "Running Production Server"
  exec uwsgi --http 0.0.0.0:9090 --wsgi-file /app/identidock.py \
       --callable app --stats 0.0.0.0:9191
fi